import RPi.GPIO as GPIO

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.OUT) #Down  K2
GPIO.setup(24, GPIO.OUT) #Up    K1

GPIO.output(23,True)
GPIO.output(24,True)

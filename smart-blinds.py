#
#	Nathan BLEUZEN 09.12.2016
#	smart-blinds.py
#	use cronjob and lircd to trigger relays on GPIO
#

# Typical use:
#	python smart-blinds.py -u

#!/usr/bin/python
import sys, getopt
import datetime, time
import os, signal
import RPi.GPIO as GPIO

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.OUT) #Down	K2
GPIO.setup(24, GPIO.OUT) #Up	K1

### INTERRUPT FUNCTION ###
def killOtherActions():
	currentPID = os.getpid()
	try:
		pidfile = open('smart-blinds/s-b.pid', 'r')
		elderPID = pidfile.read()
		elderPID = int(elderPID)
		print "An other action running (PID",elderPID,") will be killed to give the hand to",currentPID,"."
		os.kill(elderPID, signal.SIGKILL)
		reset()
	except IOError:
		print "No other action running lets proceed with this one"
		pass
	pidfile = open('smart-blinds/s-b.pid', 'w')
	pidfile.write(str(currentPID))

### CLEAN FUNCTION ###
def clean():
	print "Cleaning pid file"
	os.remove('smart-blinds/s-b.pid')

### STOP FUNCTION ###
def reset():
	if GPIO.input(23) == False:
		GPIO.output(23,True) #Down	K2
	if GPIO.input(24) == False:
		GPIO.output(24,True) #Up	K1

### MAIN ###
try:
	opts, args = getopt.getopt(sys.argv[1:],"huds", ["help", "up", "down", "stop"])
except getopt.GetoptError as err:
	print str(err)
	print "Something went wrong try: smart-blinds.py -h"
	sys.exit(2)

for opt, arg in opts:

	#HELP
	if opt in ("-h", "--help"):
		print 'testopt.py -d --down -u --up -h --help -c -r'
		sys.exit()

	#UP
	elif opt in ("-u", "--up"):
		killOtherActions()
		GPIO.output(24,False)
		time.sleep(17)
		GPIO.output(24,True)

	#DOWN
	elif opt in ("-d", "--down"):
		killOtherActions()
		GPIO.output(23,False)
		time.sleep(15.5)
		GPIO.output(23,True)

	#STOP
	elif opt in ("-s", "--stop"):
		killOtherActions()

	#OTHERWISE
	else:
		assert False, "Unhandled option, try -h"

clean()
### END MAIN ###

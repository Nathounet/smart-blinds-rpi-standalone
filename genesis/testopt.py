#
# Typical usage: python testopt.py -d down -p full
#

#!/usr/bin/python
import sys, getopt

def main():
        print "START"
        try:
                opts, args = getopt.getopt(sys.argv[1:],"huds", ["help", "up", "down", "stop"])
        except getopt.GetoptError as err:
                print str(err)
                print "Something went wrong try: testopt.py -h"
                sys.exit(2)
        for opt, arg in opts:
                if opt in ("-h", "--help"):
                        print 'testopt.py -d --down -u --up -h --help'
                        sys.exit()
                elif opt in ("-u", "--up"):
                        #start opening
                        print "Opening up"
                elif opt in ("-d", "--down"):
                        #start closing
                        print "Closing down"
                elif opt in ("-s", "--stop"):
                        #stop
                        print "Stoping"
                else:
                        assert False, "unhandled option"
        print "END"

if __name__ == "__main__":
        main()

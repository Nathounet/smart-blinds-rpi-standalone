import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(2, GPIO.OUT)
GPIO.setup(3, GPIO.IN)

# input of the switch will change the state of the LED
while True:
	GPIO.output(2,GPIO.input(3))
	time.sleep(1)

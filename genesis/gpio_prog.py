#!/usr/bin/python

import RPi.GPIO as GPIO
import sys
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(2, GPIO.OUT)
GPIO.setup(3, GPIO.OUT)

state = True
# endless loop, on/off for 1 second
while True:
        GPIO.output(2,False)
        time.sleep(0.5)
        GPIO.output(3,False)
        time.sleep(0.5)
        GPIO.output(2,True)
        time.sleep(0.5)
        GPIO.output(3,True)
        time.sleep(0.5)

        time.sleep(1)

        GPIO.output(2,False)
        time.sleep(0.25)
        GPIO.output(2,True)
        time.sleep(0.5)
        GPIO.output(3,False)
        time.sleep(0.25)
        GPIO.output(3,True)
        time.sleep(0.5)

        time.sleep(3)

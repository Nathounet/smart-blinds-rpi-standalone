#!/usr/bin/python
import RPi.GPIO as GPIO
import time
import sys

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(2, GPIO.OUT)
GPIO.setup(3, GPIO.OUT)

#Log args
print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv)

#dictionnary def
direction={'up': 1, 'down': 2}
position={'open': 1, 'closed': 2, 'full': 3}

#act
if direction[sys.argv[1]] == 1:
	print "Opening the curtains"
        GPIO.output(3,False)
#        time.sleep(20)
        GPIO.output(3,True)
elif direction[sys.argv[1]] == 2:
	if position[sys.argv[2]] == 1:
		print "Closing the curtains until", sys.argv[2]
		GPIO.output(2,False)
#	        time.sleep(15)
        	GPIO.output(2,True)
	elif position[sys.argv[2]] == 2:
                print "Closing the curtains until", sys.argv[2]
		GPIO.output(2,False)
#	        time.sleep(16.5)
        	GPIO.output(2,True)
	elif position[sys.argv[2]] == 3:
                print "Closing the curtains until", sys.argv[2]
        	GPIO.output(2,False)
#	        time.sleep(20)
        	GPIO.output(2,True)
	else:
		print "Please use a valid position command: 'a little open', 'closed' or 'fully closed'"
else:
	print "Please use a valid direction command: 'up' or 'down'"

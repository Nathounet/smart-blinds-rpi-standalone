#!/bin/bash

#	$ ps aux | grep irexec
#	$ kill -9 PID

echo "Launching irexec"
irexec & >> /home/pi/smart-blinds/logs/irexec.log 2>&1
echo $! >> /home/pi/smart-blinds/logs/irexec.log
echo "irexec PID:" $!
